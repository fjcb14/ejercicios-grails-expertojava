package es.ua.expertojava.todo

class Tag {

    String name
    String color = "#020202"

    static hasMany = [todos:Todo]

    static constraints = {
        name(blank:false, nullable:true, unique:true)
        color(shared: "rgbcolor")
    }

    String toString(){
        name
    }
}