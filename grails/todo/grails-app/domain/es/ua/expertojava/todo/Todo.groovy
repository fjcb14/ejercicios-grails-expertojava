package es.ua.expertojava.todo

class Todo {
    String title
    String url
    Date date
    Date reminderDate
    Boolean state
    Category category
    String description
    User user
    Date dateCreated
    Date lastUpdated
    Date dateDone

    static searchable = true

    static hasMany = [tags:Tag]
    static belongsTo = [Tag, User]

    static constraints = {
        title(blank:false)
        url(nullable:true, url:true)
        date(nullable:false)
        //reminderDate(nullable:true)
        reminderDate(nullable:true, validator: {
            val, obj ->
                if (val && obj.date) {
                    return val.before(obj.date)
                }
                return true
            }
        )
        state(nullable:false)
        category(nullable:true)
        description(blank:true, nullable:true, maxSize:1000)
        user(nullable: true)
        dateDone(nullable: true)
    }

    String toString(){
        title
    }


}