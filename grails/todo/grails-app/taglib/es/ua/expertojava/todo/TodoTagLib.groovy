package es.ua.expertojava.todo

class TodoTagLib {
    static defaultEncodeAs = [taglib:'none']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = 'todo'

    def printIconFromBoolean = { state ->
        if(state.value) {
            out << asset.image(src:"ok.png", alt:"ok", width:35, eight:35)
        }else{
            out << asset.image(src:"ko.png", alt:"ko", width:35, eight:35)
        }
    }
}
