package todo

class LogFilters {

    def springSecurityService

    def filters = {
        all(controller:'todo|category|tag', action:'*') {
            before = {

            }
            after = { Map model ->
                log.trace("Usuario ${springSecurityService.currentUser} - Controlador ${controllerName} - Accion ${actionName} - Modelo ${model}")
            }
            afterView = { Exception e ->

            }
        }
    }
}
