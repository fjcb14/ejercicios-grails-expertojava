import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->
        def categoryHome = new Category(name:"Hogar").save()
        def categoryJob = new Category(name:"Trabajo").save()

        def tagEasy = new Tag(name:"Fácil").save()
        def tagDifficult = new Tag(name:"Difícil").save()
        def tagArt = new Tag(name:"Arte").save()
        def tagRoutine = new Tag(name:"Rutina").save()
        def tagKitchen = new Tag(name:"Cocina").save()

        def todoPaintKitchen = new Todo(title:"Pintar cocina", date:new Date()+1, state: false)
        def todoCollectPost = new Todo(title:"Recoger correo postal", date:new Date()+2, state: false)
        def todoBakeCake = new Todo(title:"Cocinar pastel", date:new Date()+4, state: false)
        def todoWriteUnitTests = new Todo(title:"Escribir tests unitarios", date:new Date(), state: false)

        todoPaintKitchen.addToTags(tagDifficult)
        todoPaintKitchen.addToTags(tagArt)
        todoPaintKitchen.addToTags(tagKitchen)
        todoPaintKitchen.category = categoryHome
        todoPaintKitchen.save()

        todoCollectPost.addToTags(tagRoutine)
        todoCollectPost.category = categoryJob
        todoCollectPost.save()

        todoBakeCake.addToTags(tagEasy)
        todoBakeCake.addToTags(tagKitchen)
        todoBakeCake.category = categoryHome
        todoBakeCake.save()

        todoWriteUnitTests.addToTags(tagEasy)
        todoWriteUnitTests.category = categoryJob
        todoWriteUnitTests.save()


        def Role rollAdmin = new Role(authority: "ROLE_ADMIN").save()
        def Role rollBasic = new Role(authority: "ROLE_BASIC").save()

        def User admin = new User(username: "admin", password: "admin", name: "Fran", surnames: "Cano", confirmPassword: "admin", email: "fran@mail.com").save()
        def User user1 = new User(username: "usuario1", password: "usuario1", name: "Ramon", surnames: "Jimenez", confirmPassword: "usuario1", email: "ramon@mail.com").save()
        def User user2 = new User(username: "usuario2", password: "usuario2", name: "Julia", surnames: "Sanchez", confirmPassword: "usuario2", email: "julia@mail.com").save()

        PersonRole.create admin, rollAdmin, true
        PersonRole.create user1, rollBasic, true
        PersonRole.create user2, rollBasic, true

        todoPaintKitchen.user = user1
        todoPaintKitchen.save()
        todoCollectPost.user = user2
        todoCollectPost.save()
        todoBakeCake.user = user2
        todoBakeCake.save()
        todoWriteUnitTests.user = user1
        todoWriteUnitTests.save()
    }
    def destroy = { }
}