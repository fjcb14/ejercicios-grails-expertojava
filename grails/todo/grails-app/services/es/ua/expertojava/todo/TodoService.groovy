package es.ua.expertojava.todo

import grails.transaction.Transactional
import groovy.time.TimeCategory

@Transactional
class TodoService {

    def springSecurityService

    def serviceMethod() {

    }

    def deleteTags(Todo todoInstance) {
        def tafDelTodo = todoInstance.tags.findAll()
        tafDelTodo?.each {tag ->
            todoInstance.removeFromTags(tag)
        }
    }

    def saveTodo(Todo todoInstance){
        if(todoInstance.state){
            todoInstance.setDateDone(todoInstance.getLastUpdated())
        }
    }

    def lastTodosDone(Integer hours){
        Date fechaActual = new Date()
        Date fechaInicio = new Date()
        use(TimeCategory){
            fechaInicio = fechaActual - hours.hours
        }
        Todo.findAllByDateDoneBetween(fechaInicio,  fechaActual)
    }

    def asignarUsuarioTarea(Todo todoInstance){
        def usuario = springSecurityService.currentUser
        todoInstance.setUser(usuario)
    }
}
