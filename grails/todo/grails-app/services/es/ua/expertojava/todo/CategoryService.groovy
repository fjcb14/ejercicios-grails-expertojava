package es.ua.expertojava.todo

import grails.transaction.Transactional

@Transactional
class CategoryService {

    def serviceMethod() {

    }

    def deleteTodo(Category categoryInstance) {
        //recuperamos todas las tareas que tengan esta Categoria y la desvinculamos
        categoryInstance.getTodos().each {todo ->
            todo.setCategory(null)
        }
    }
}
