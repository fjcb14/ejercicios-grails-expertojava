package es.ua.expertojava.todo

import grails.web.RequestParameter

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TodoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def todoService
    def springSecurityService
    def searchableService

    def index(Integer max) {
        def usuario = springSecurityService.currentUser

        params.max = Math.min(max ?: 10, 100)
        //respond Todo.list(params), model:[todoInstanceCount: Todo.count()]
        respond Todo.findAllByUser(usuario, params), model:[todoInstanceCount: Todo.count()]
    }

    def show(Todo todoInstance) {
        respond todoInstance
    }

    def create() {
        respond new Todo(params)
    }

    @Transactional
    def save(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'create'
            return
        }

        todoService.saveTodo(todoInstance)
        todoService.asignarUsuarioTarea(todoInstance)

        todoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*' { respond todoInstance, [status: CREATED] }
        }
    }

    def edit(Todo todoInstance) {
        respond todoInstance
    }

    @Transactional
    def update(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'edit'
            return
        }

        todoService.saveTodo(todoInstance)

        todoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*'{ respond todoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Todo todoInstance) {

        if (todoInstance == null) {
            notFound()
            return
        }

        todoService.deleteTags(todoInstance)

        todoInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def listByCategory(){
        respond Category.list(params)
    }

    def informePorCategory(Integer max){
        List listaCategorias = new ArrayList()
        def categoria
        def usuario = springSecurityService.currentUser

        if(params.categoria instanceof String){
            categoria = Category.findByName(params.categoria)
        }else{
            for(String s: params.categoria){
                println "nombre categorya" + s
                listaCategorias.add(Category.findByName(s))
            }
        }

        params.sort = 'date'
        params.order = 'desc'
        params.max = Math.min(max ?: 10, 100)

        if(listaCategorias.size()>1){
            def tareasFiltradas = Todo.findAllByCategoryInListAndUser(listaCategorias, usuario, params)
            respond tareasFiltradas, model:[todoInstanceCount: tareasFiltradas.size()]
        }else{
            def tareasFiltradas = Todo.findAllByCategoryAndUser(categoria, usuario, params)
            respond tareasFiltradas, model:[todoInstanceCount: tareasFiltradas.size()]
        }
    }

    def showTodosByUser(){
        def user = User.findByUsername(params.get("username"))
        def tareasUsuario = Todo.findAllByUser(user)
        respond tareasUsuario, model:[todoInstanceCount: tareasUsuario.size()]
    }

    def searchable(){

        def busqueda = Todo.search([result:'every']) {
            must(queryString(params.get("cadenaABuscar")))
            must(term('$/Todo/user/id', User.find(springSecurityService.currentUser)?.id))
        }

        respond busqueda, model:[todoInstanceCount: busqueda.size()], view: "index"
    }

}
