<!DOCTYPE html>
<html>
    <head>
        <title>Confirmacion de borrado</title>
        <meta name="layout" content="main">
    </head>
    <body>
        <p>
            <asset:image src="warning.png" alt="warning" width="100" eight="100" align="center"/>¿Esta seguro de que desea borrar la etiqueta?
        </p>
        <g:form url="[resource:tagInstance, action:'delete']" method="DELETE">
            <fieldset class="buttons">
                <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" />
            </fieldset>
        </g:form>
    </body>
</html>
