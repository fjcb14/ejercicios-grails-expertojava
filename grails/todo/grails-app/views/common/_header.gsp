<div id="header">
    <div id="menu">
        <nobr>
            <sec:ifLoggedIn>
                <p>
                    <b><sec:loggedInUserInfo field="username"/></b>
                    <g:form name="logout" controller="logout" action="index" method="POST">
                        <g:submitButton name="logout" class="index"/>
                    </g:form>
                </p>
            </sec:ifLoggedIn>

            <sec:ifNotLoggedIn>
                <g:link controller="login" action="auth">Login</g:link>
            </sec:ifNotLoggedIn>
        </nobr>
    </div>
</div>
