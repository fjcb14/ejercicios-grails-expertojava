class Libro {
    /* Añade aquí la definición de la clase */
    String nombre
    int anyo
    String autor
    String editorial
    
    Libro(nombreEntrada, anyoEntrada, autorEntrada){
        nombre = nombreEntrada
        anyo = anyoEntrada
        autor = autorEntrada
    }
    
    String getAutor(){
        def (apellido, nombre) = autor.tokenize(',')
        return "${nombre.trim()} $apellido"
    }
}

/* Crea aquí las tres instancias de libro l1, l2 y l3 */
l1 = new Libro("La colmena", 1951, "Cela Trulock, Camilo José")
l2 = new Libro("La galatea", 1585, "de Cervantes Saavedra, Miguel")
l3 = new Libro("La dorote", 1632, "Lope de Vega y Carpio, Félix Arturo")

assert l1.getNombre() == 'La colmena'
assert l2.getAnyo() == 1585
assert l3.getAutor() == 'Félix Arturo Lope de Vega y Carpio'

/* Añade aquí la asignación de la editorial a todos los libros */

l1.setEditorial("Anaya")
l2.setEditorial("Planeta")
l3.setEditorial("Santillana")

assert l1.getEditorial() == 'Anaya'
assert l2.getEditorial() == 'Planeta'
assert l3.getEditorial() == 'Santillana'

return