class Calculadora{

    static def operador1
    static def operador2
    static def operacion
    
    //Definimos la logica de calculo
    static def calcular(){
        def resultado = 0
        switch (operacion) {
            case "+":
                resultado = Integer.parseInt(operador1)+Integer.parseInt(operador2)
                break
            case "-":
                resultado = Integer.parseInt(operador1)-Integer.parseInt(operador2)
                break
            case "*":
                resultado = Integer.parseInt(operador1)*Integer.parseInt(operador2)
                break
            case "/":
                resultado = Integer.parseInt(operador1)/Integer.parseInt(operador2)
                break
            default:
                break
        }
    }

    //Solicitamos los datos utilizando un clousure
    static void main(String... args) {
        def solicitarDatos = System.in.withReader { it ->
            print 'Introduzca operador 1: '
            operador1 = it.readLine()
            print 'Introduzca operador 2: '
            operador2 = it.readLine()
            print 'Introduzca el operador (Validos: +-*/): '
            operacion = it.readLine()
        }
        println "Calculando: " + operador1 + operacion + operador2 + " Resultado: " + calcular()
    }
}