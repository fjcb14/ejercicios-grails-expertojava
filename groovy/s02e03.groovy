/* Añade aquí la implementación solicitada en el ejercicio */

/*Voy a utilizar la metaclass para crear un nuevo metodo en ejecucion. 
Dentro evaluare la localizacion y usare el valor contenido en el delegate para devolve la cadena buscada*/
Number.metaClass.moneda = { localizacion ->
    switch (localizacion) {
        case "es_ES":
            return "${delegate}€"
            break
        case "en_US":
            return "\$${delegate}"
            break
        case "en_EN":
            return "£${delegate}"
            break
    }
}

assert 10.2.moneda("en_EN") ==  "£10.2"
assert 10.2.moneda("en_US") ==  "\$10.2"
assert 10.2.moneda("es_ES") ==  "10.2€"

assert 10.moneda("en_EN") ==  "£10"
assert 10.moneda("en_US") ==  "\$10"
assert 10.moneda("es_ES") ==  "10€"

assert new Float(10.2).moneda("en_EN") ==  "£10.2"
assert new Float(10.2).moneda("en_US") ==  "\$10.2"
assert new Float(10.2).moneda("es_ES") ==  "10.2€"

assert new Double(10.2).moneda("en_EN") ==  "£10.2"
assert new Double(10.2).moneda("en_US") ==  "\$10.2"
assert new Double(10.2).moneda("es_ES") ==  "10.2€"