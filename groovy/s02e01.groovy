/* Añade aquí la implementación del factorial en un closure */

//recorremos el numero descendentemente hasta llegar a 1 calculando el factorial
def factorial = {numero -> 
    def total = 1
    numero.downto(1) { 
        num -> 
        total = total*num
    }
    return total
}
 
assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

//Hacemos una lista con los numeros, la recorremos e invocamos al Clousure
def listaNumeros = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15].collect { num -> factorial(num) }
//momstramos la lista
println listaNumeros


/* Añade a partir de aquí el resto de tareas solicitadas en el ejercicios */

//creamos los 
def ayer = { fecha ->
    def fechaAyer = new Date().parse('d/M/yyyy H:m:s', fecha) - 1
    return fechaAyer.format('d/M/yyyy H:m:s')
}

def manyana = { fecha ->
    def fechaManyana = new Date().parse('d/M/yyyy H:m:s', fecha) + 1
    return fechaManyana.format('d/M/yyyy H:m:s')
}

//definimos una lista de fechas y probamos
def fechas = ["15/2/2016 00:30:20","31/12/2015 00:30:20"]
def fechasAyer =fechas.collect{ayer(it)}
def fechasManyana =fechas.collect{manyana(it)}

println fechasAyer
println fechasManyana